@extends('layouts.main')

@section('content')

    <h1 class="h2">Base Data</h1>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <h5 class="card-header"> {{ Html::link(route('assets.index'),"Back") }} | Asset No. {{ $asset->asst_no }}</h5>
                <div class="card-body">
                    
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>LACO CODE</th>
                                    <td>{{ $asset->laco_code }}</td>
                                </tr>
                                <tr>
                                    <th>Asset No.</th>
                                    <td>{{ $asset->asst_no }}</td>
                                </tr>
                                <tr>
                                    <th>Computer Name</th>
                                    <td>{{ $asset->com_name }}</td>
                                </tr>
                                <tr>
                                    <th>Owner</th>
                                    <td>{{ $asset->user_name }}</td>
                                </tr>
                                <tr>
                                    <th>Dep</th>
                                    <td>{{ $asset->Dep }}</td>
                                </tr>
                                <tr>
                                    <th>Service Tag</th>
                                    <td>{{ $asset->service_tag }}</td>
                                </tr>
                                <tr>
                                    <th>LOB DESC</th>
                                    <td>{{ $asset->LOB }} / {{ $asset->DESC }}</td>
                                </tr>
                                <tr>
                                    <th>ASSET SHIP DATE</th>
                                    <td>{{ $asset->asset_ship_date }}</td>
                                </tr>
                                <tr>
                                    <th>CONTRACT END DATE</th>
                                    <td>{{ $asset->contract_end_date }}</td>
                                </tr>
                                <tr>
                                    <th>ปีเริ่มประกัน</th>
                                    <td>{{ $asset->start_year }}</td>
                                </tr>
                                <tr>
                                    <th>อายุเครื่อง</th>
                                    <td>{{ date('Y') - $asset->start_year }}</td>
                                </tr>
                                <tr>
                                    <th>สถานะ</th>
                                    <td>{{ $asset->com_status }}</td>
                                </tr>
                                <tr>
                                    <th>MSP</th>
                                    <td>{{ $asset->use_msp }}</td>
                                </tr>
                                <tr>
                                    <th>MSP Install DATE</th>
                                    <td>{{ $asset->msp_install_date }}</td>
                                </tr>
                                <tr>
                                    <th>MAC Address</th>
                                    <td>{{ $asset->mac_addr }}</td>
                                </tr>
                                
                                <tr>
                                    <th>OS</th>
                                    <td>{{ $asset->win_ver }} / {{ $asset->win_key }}</td>
                                </tr>
                                
                                <tr>
                                    <th>MS Office</th>
                                    <td>{{ $asset->office_ver }}</td>
                                </tr>
                                <tr>
                                    <th>Location</th>
                                    <td>{{ $asset->location }}</td>
                                </tr>
                                <tr>
                                    <th>Note</th>
                                    <td>{{ $asset->note }}</td>
                                </tr>
                                <tr>
                                    <th>Status</th>
                                    <td>{{ $asset->status }}</td>
                                </tr>
                            </tbody>
                        </table>
                     </div>
                </div>
            </div>
        </div>
    </div>

@endsection