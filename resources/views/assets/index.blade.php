@extends('layouts.main')

@section('content')

    <h1 class="h2">Base Data</h1>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <h5 class="card-header">Asset
                    <form method="GET" action="{{ route('assets.index') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        Search
                                    </button>
                                </span>
                            </div>
                        </form>

                </h5>
                <div class="card-body">
                    
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Asset No.</th>
                                    <th scope="col">Computer Name</th>
                                    <th scope="col">Dep</th>
                                    <th scope="col">Service Tag</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($assets as $asset)
                                <tr>
                                    <td>{{ $asset->asst_no }}</td>
                                    <td>{{ $asset->com_name }}</td>
                                    <td>{{ $asset->Dep }}</td>
                                    <td>{{ $asset->service_tag }}</td>
                                    <td>{{ $asset->status }}</td>
                                    <td>{{ Html::link(route('assets.show',$asset->id),"View") }}                                        
                                    </td>
                                </tr>    
                                @endforeach
                                
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $assets->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection