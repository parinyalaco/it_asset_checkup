@extends('layouts.main')

@section('content')

    <h1 class="h2">Activity</h1>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <h5 class="card-header"> {{ Html::link(route('checkups.show',$checkupd->check_up_m_id), 'Back') }} | ดำเนินการตรวจสอบ</h5>
                <div class="card-body">
                    <form method="POST" action="{{ route('checkups.doitaction',$checkupd->id) }}" accept-charset="UTF-8" enctype="multipart/form-data">
                        @csrf
                        {{ Form::hidden('status','Created') }}
                        <div class="row">
                            <div class="col-3">
                                <label for="act_date" class="form-label">Act Date</label>
                                @if (!empty($checkupd->act_date))
                                    {{ Form::date('act_date',$checkupd->act_date,['class'=>'form-control','placeholder'=>'ใส่ชื่อ','required'=>true]) }}
                                @else
                                    {{ Form::date('act_date',null,['class'=>'form-control','placeholder'=>'ใส่ชื่อ','required'=>true]) }}
                                @endif                                
                            </div>       
                            <div class="col-3">
                                <label for="display" class="form-label">หน้าจอ</label>
                                @if (!empty($checkupd->display))                                
                                    {{ Form::text('display',$checkupd->display,['class'=>'form-control','placeholder'=>'ปรกติหรือไม่ปรกติ','required'=>true]) }}
                                @else
                                    {{ Form::text('display',null,['class'=>'form-control','placeholder'=>'ปรกติหรือไม่ปรกติ','required'=>true]) }}
                                @endif 
                            </div>    
                            <div class="col-3">
                                <label for="body" class="form-label">ตัวเครื่อง หรือ เคส</label>
                                @if (!empty($checkupd->body))
                                    {{ Form::text('body',$checkupd->body,['class'=>'form-control','placeholder'=>'ปรกติหรือไม่ปรกติ','required'=>true]) }}
                                @else
                                    {{ Form::text('body',null,['class'=>'form-control','placeholder'=>'ปรกติหรือไม่ปรกติ','required'=>true]) }}
                                @endif
                                
                            </div>     
                            <div class="col-3">
                                <label for="fan" class="form-label">พัดลมตัวเครื่อง</label>
                                @if (!empty($checkupd->fan))
                                    {{ Form::text('fan',$checkupd->fan,['class'=>'form-control','placeholder'=>'ปรกติหรือไม่ปรกติ','required'=>true]) }}
                                @else
                                    {{ Form::text('fan',null,['class'=>'form-control','placeholder'=>'ปรกติหรือไม่ปรกติ','required'=>true]) }}
                                @endif
                                
                            </div>  
                            <div class="col-3">
                                <label for="keyboard" class="form-label">Keyboard</label>
                                @if (!empty($checkupd->keyboard))
                                    {{ Form::text('keyboard',$checkupd->keyboard,['class'=>'form-control','placeholder'=>'ปรกติหรือไม่ปรกติ','required'=>true]) }} 
                                @else
                                   {{ Form::text('keyboard',null,['class'=>'form-control','placeholder'=>'ปรกติหรือไม่ปรกติ','required'=>true]) }} 
                                @endif
                                
                            </div>   
                            <div class="col-3">
                                <label for="mouse" class="form-label">Mouse</label>
                                @if (!empty($checkupd->mouse))
                                    {{ Form::text('mouse',$checkupd->mouse,['class'=>'form-control','placeholder'=>'ปรกติหรือไม่ปรกติ','required'=>true]) }}
                                @else
                                    {{ Form::text('mouse',null,['class'=>'form-control','placeholder'=>'ปรกติหรือไม่ปรกติ','required'=>true]) }}
                                @endif
                                
                            </div>  
                            <div class="col-3">
                                <label for="usb" class="form-label">USB</label>
                                @if (!empty($checkupd->usb))
                                    {{ Form::text('usb',$checkupd->usb,['class'=>'form-control','placeholder'=>'ปรกติหรือไม่ปรกติ','required'=>true]) }}
                                @else
                                    {{ Form::text('usb',null,['class'=>'form-control','placeholder'=>'ปรกติหรือไม่ปรกติ','required'=>true]) }}
                                @endif
                                
                            </div>   
                            <div class="col-3">
                                <label for="vga" class="form-label">ช่องต่อ VGA</label>
                                @if (!empty($checkupd->vga))
                                     {{ Form::text('vga',$checkupd->vga,['class'=>'form-control','placeholder'=>'ปรกติหรือไม่ปรกติ','required'=>true]) }}
                                @else
                                    {{ Form::text('vga',null,['class'=>'form-control','placeholder'=>'ปรกติหรือไม่ปรกติ','required'=>true]) }}
                                @endif
                                
                            </div>  
                            <div class="col-3">
                                <label for="hdmi" class="form-label">ช่องต่อ HDMI</label>
                                @if (!empty($checkupd->hdmi))
                                    {{ Form::text('hdmi',$checkupd->hdmi,['class'=>'form-control','placeholder'=>'ปรกติหรือไม่ปรกติ','required'=>true]) }}
                                @else
                                    {{ Form::text('hdmi',null,['class'=>'form-control','placeholder'=>'ปรกติหรือไม่ปรกติ','required'=>true]) }}
                                @endif
                                
                            </div> 
                            <div class="col-3">
                                <label for="usbc" class="form-label">USB-C</label>
                                @if (!empty($checkupd->usbc))
                                    {{ Form::text('usbc',$checkupd->usbc,['class'=>'form-control','placeholder'=>'ปรกติหรือไม่ปรกติ','required'=>true]) }} 
                                @else
                                   {{ Form::text('usbc',null,['class'=>'form-control','placeholder'=>'ปรกติหรือไม่ปรกติ','required'=>true]) }} 
                                @endif
                                
                            </div> 
                            <div class="col-3">
                                <label for="cardreader" class="form-label">ช่อง Card reader</label>
                                @if (!empty($checkupd->cardreader))
                                    {{ Form::text('cardreader',$checkupd->cardreader,['class'=>'form-control','placeholder'=>'ปรกติหรือไม่ปรกติ','required'=>true]) }}
                                @else
                                    {{ Form::text('cardreader',null,['class'=>'form-control','placeholder'=>'ปรกติหรือไม่ปรกติ','required'=>true]) }}
                                @endif
                                
                            </div> 
                            <div class="col-3">
                                <label for="lan" class="form-label">Lan</label>
                                @if (!empty($checkupd->lan))
                                    {{ Form::text('lan',$checkupd->lan,['class'=>'form-control','placeholder'=>'ปรกติหรือไม่ปรกติ','required'=>true]) }}
                                @else
                                    {{ Form::text('lan',null,['class'=>'form-control','placeholder'=>'ปรกติหรือไม่ปรกติ','required'=>true]) }}
                                @endif
                                
                            </div> 
                            <div class="col-3">
                                <label for="wifi" class="form-label">Wifi</label>
                                @if (!empty($checkupd->wifi))
                                    {{ Form::text('wifi',$checkupd->wifi,['class'=>'form-control','placeholder'=>'ปรกติหรือไม่ปรกติ','required'=>true]) }}
                                @else
                                    {{ Form::text('wifi',null,['class'=>'form-control','placeholder'=>'ปรกติหรือไม่ปรกติ','required'=>true]) }}
                                @endif
                                
                            </div>                             
                            <div class="col-3">
                                <label for="hardware_other" class="form-label">Hardware อื่นๆ</label>
                                @if (!empty($checkupd->hardware_other))
                                    {{ Form::text('hardware_other',$checkupd->hardware_other,['class'=>'form-control','placeholder'=>'ปรกติหรือไม่ปรกติ','required'=>true]) }}
                                @else
                                    {{ Form::text('hardware_other',null,['class'=>'form-control','placeholder'=>'ปรกติหรือไม่ปรกติ','required'=>true]) }}
                                @endif
                                
                            </div> 
                                       
                                                     
                            <div class="col-3">
                                <label for="os" class="form-label">OS</label>
                                @if (!empty($checkupd->os))
                                    {{ Form::text('os',$checkupd->os,['class'=>'form-control','placeholder'=>'ปรกติหรือไม่ปรกติ','required'=>true]) }}
                                @else
                                    {{ Form::text('os',null,['class'=>'form-control','placeholder'=>'ปรกติหรือไม่ปรกติ','required'=>true]) }}
                                @endif
                                
                            </div> 
                            <div class="col-3">
                                <label for="msp" class="form-label">MSP</label>
                                @if (!empty($checkupd->msp))
                                    {{ Form::text('msp',$checkupd->msp,['class'=>'form-control','placeholder'=>'ปรกติหรือไม่ปรกติ','required'=>true]) }}
                                @else
                                    {{ Form::text('msp',null,['class'=>'form-control','placeholder'=>'ปรกติหรือไม่ปรกติ','required'=>true]) }}
                                @endif
                                
                            </div>
                            
                            <div class="col-3">
                                <label for="software_note" class="form-label">Software อื่นๆ</label>
                                @if (!empty($checkupd->software_note))
                                    {{ Form::text('software_note',$checkupd->software_note,['class'=>'form-control','placeholder'=>'ปรกติหรือไม่ปรกติ','required'=>true]) }}
                                @else
                                    {{ Form::text('software_note',null,['class'=>'form-control','placeholder'=>'ปรกติหรือไม่ปรกติ','required'=>true]) }}
                                @endif
                                
                            </div>
                            <div class="col-9">
                                <label for="summary_note" class="form-label">สรุปรวม</label>
                                @if (!empty($checkupd->summary_note))
                                    {{ Form::text('summary_note',$checkupd->summary_note,['class'=>'form-control','placeholder'=>'ปรกติหรือไม่ปรกติ','required'=>true]) }}
                                @else
                                    {{ Form::text('summary_note',null,['class'=>'form-control','placeholder'=>'ปรกติหรือไม่ปรกติ','required'=>true]) }}
                                @endif
                                
                            </div>
                            <div class="col-4">
                                <label for="software_note" class="form-label">ภาพด้านหน้า</label>
                                {!! Form::file('pic1f', $attributes = ['accept' => 'image/jpeg , image/jpg, image/gif, image/png']) !!}
                            </div>
                            <div class="col-4">
                                <label for="software_note" class="form-label">ภาพด้านหลัง</label>
                                {!! Form::file('pic2f', $attributes = ['accept' => 'image/jpeg , image/jpg, image/gif, image/png']) !!}
                            </div>
                            <div class="col-4">
                                <label for="software_note" class="form-label">ภาพอุปกรณ์ต่อพ่วง</label>
                                {!! Form::file('pic3f', $attributes = ['accept' => 'image/jpeg , image/jpg, image/gif, image/png']) !!}
                            </div>  
                            <div class="col-4">
                                <label for="software_note" class="form-label">ภาพอุปกรณ์ที่อาจจะเสีย</label>
                                {!! Form::file('pic4f', $attributes = ['accept' => 'image/jpeg , image/jpg, image/gif, image/png']) !!}
                            </div>
                             
                            <div class="col-4">
                                <label for="software_note" class="form-label">ภาพอื่นๆ</label>
                                {!! Form::file('pic5f', $attributes = ['accept' => 'image/jpeg , image/jpg, image/gif, image/png']) !!}
                            </div>
                            <div class="col-4">
                                {{ Form::submit('ส่งผล',['class'=>'btn btn-success']) }}
                            </div>
                        </div>
                    </form>
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>รอบการตรวจเช็ค / ปี</th>
                                    <td>{{ $checkupd->checkupm->name }} / {{ $checkupd->checkupm->year }}</td>
                               
                                    <th>สถานะการตรวจสอบ</th>
                                    <td>{{ $checkupd->status }}</td>
                                </tr>
                                <tr>
                                    <th>LACO CODE</th>
                                    <td>{{ $checkupd->asset->laco_code }}</td>
                               
                                    <th>Asset No.</th>
                                    <td>{{ $checkupd->asset->asst_no }}</td>
                                </tr>
                                <tr>
                                    <th>Computer Name</th>
                                    <td>{{ $checkupd->asset->com_name }}</td>
                                
                                    <th>Owner</th>
                                    <td>{{ $checkupd->asset->user_name }}</td>
                                </tr>
                                <tr>
                                    <th>Dep</th>
                                    <td>{{ $checkupd->asset->Dep }}</td>
                               
                                    <th>Service Tag</th>
                                    <td>{{ $checkupd->asset->service_tag }}</td>
                                </tr>
                                <tr>
                                    <th>LOB DESC</th>
                                    <td>{{ $checkupd->asset->LOB }} / {{ $checkupd->asset->DESC }}</td>
                                
                                    <th>ASSET SHIP DATE</th>
                                    <td>{{ $checkupd->asset->asset_ship_date }}</td>
                                </tr>
                                <tr>
                                    <th>CONTRACT END DATE</th>
                                    <td>{{ $checkupd->asset->contract_end_date }}</td>
                                
                                    <th>ปีเริ่มประกัน</th>
                                    <td>{{ $checkupd->asset->start_year }}</td>
                                </tr>
                                <tr>
                                    <th>อายุเครื่อง</th>
                                    <td>{{ date('Y') - $checkupd->asset->start_year }}</td>
                                
                                    <th>สถานะ</th>
                                    <td>{{ $checkupd->asset->com_status }}</td>
                                </tr>
                                <tr>
                                    <th>MSP</th>
                                    <td>{{ $checkupd->asset->use_msp }}</td>
                                
                                    <th>MSP Install DATE</th>
                                    <td>{{ $checkupd->asset->msp_install_date }}</td>
                                </tr>
                                <tr>
                                    <th>MAC Address</th>
                                    <td>{{ $checkupd->asset->mac_addr }}</td>
                                
                                    <th>OS</th>
                                    <td>{{ $checkupd->asset->win_ver }} / {{ $checkupd->asset->win_key }}</td>
                                </tr>
                                
                                <tr>
                                    <th>MS Office</th>
                                    <td>{{ $checkupd->asset->office_ver }}</td>
                                
                                    <th>Location</th>
                                    <td>{{ $checkupd->asset->location }}</td>
                                </tr>
                                <tr>
                                    <th>Note</th>
                                    <td>{{ $checkupd->asset->note }}</td>
                                
                                    <th>Status</th>
                                    <td>{{ $checkupd->asset->status }}</td>
                                </tr>
                            </tbody>
                        </table>
                     </div>
                    
                </div>
            </div>
        </div>
    </div>
@endsection
