@extends('layouts.main')

@section('content')

    <h1 class="h2">Activity</h1>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <h5 class="card-header">Plan {{ $checkupm->name }} / {{ $checkupm->year }}
                    <form method="GET" action="{{ route('checkups.show',$checkupm->id) }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        Search
                                    </button>
                                </span>
                            </div>
                        </form>

                </h5>
                <div class="card-body">
                    {{ Html::link(url('/checkups/show/'.$checkupm->id.'/?status='),"All",['class'=>'btn btn-primary']) }}   
                    {{ Html::link(url('/checkups/show/'.$checkupm->id.'/?status=Pending'),"Pending",['class'=>'btn btn-success']) }}   
                    {{ Html::link(url('/checkups/show/'.$checkupm->id.'/?status=Planned'),"Planned",['class'=>'btn btn-primary']) }}                       
                    {{ Html::link(url('/checkups/show/'.$checkupm->id.'/?status=Done'),"Done",['class'=>'btn btn-success']) }}   
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Plan / Act</th>
                                    <th scope="col">Asset No. / Service Tag</th>
                                    <th scope="col">Computer Name / Dep</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($checkupds as $checkupd)
                                <tr><td>
                                    @if (!empty($checkupd->plan_date) or !empty($checkupd->act_date))
                                        {{ $checkupd->plan_date }} / {{ $checkupd->act_date }}
                                    @else
                                        {{ Html::link(route('checkups.setplan',$checkupd->id),"ตั้งแผน",['class'=>'btn btn-primary']) }}   
                                    @endif
                                    </td>
                                    <td>{{ $checkupd->asset->asst_no }} / {{ $checkupd->asset->service_tag }}</td>
                                    <td>{{ $checkupd->asset->com_name }} / {{ $checkupd->asset->Dep }}</td>                                    
                                    <td>{{ $checkupd->status }}</td>
                                    <td>
                                    @if ($checkupd->status == 'Planned')
                                        {{ Html::link(route('checkups.doit',$checkupd->id),"Do It",['class'=>'btn btn-primary']) }}   
                                    @else
                                        
                                    @endif    
                                    </td>
                                </tr>    
                                @endforeach
                                
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $checkupds->appends(['search' => Request::get('search'),'status' => Request::get('status')])->render() !!} </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection