@extends('layouts.main')

@section('content')

    <h1 class="h2">Base Data</h1>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <h5 class="card-header"> {{ Html::link(route('checkups.index'), 'Back') }} | Create New Plan</h5>
                <div class="card-body">
                    <form method="POST" action="{{ route('checkups.createplanaction') }}">
                        @csrf
                        {{ Form::hidden('status','Created') }}
                        <div class="row">
                            <div class="col-6">
                                <label for="name" class="form-label">Name</label>
                                {{ Form::text('name',null,['class'=>'form-control','placeholder'=>'ใส่ชื่อ','required'=>true]) }}
                            </div>                            
                            <div class="col-6">
                                <label for="year" class="form-label">Year</label>
                                {{ Form::number('year',date('Y'),['class'=>'form-control','required'=>true]) }}
                            </div>
                            <div class="col-12">
                                <label for="desc" class="form-label">Desc</label>
                                {{ Form::textarea('desc',null,['class'=>'form-control','placeholder'=>'ใส่รายบะเอียด']) }}
                            </div>                            
                            <div class="col-12">
                                {{ Form::submit('สร้าง',['class'=>'btn btn-success']) }}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
