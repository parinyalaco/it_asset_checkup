@extends('layouts.main')

@section('content')

    <h1 class="h2">Activities</h1>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <h5 class="card-header">Check Up {{ Html::link(route('checkups.createplan'), 'Create', ['class' => 'btn btn-primary']) }}

                </h5>
                <div class="card-body">
                    
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Year</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Numbers</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($checkupms as $checkup)
                                <tr>
                                    <td>{{ $checkup->year }}</td>
                                    <td>{{ $checkup->name }}</td>
                                    <td>{{ $checkup->checkupds->count() }}</td>
                                    <td>{{ $checkup->status }}</td>
                                    <td>{{ Html::link(route('checkups.show',$checkup->id),"View") }}                                        
                                    </td>
                                </tr>    
                                @endforeach                                
                            </tbody>
                        </table>
                        <div class="pagination-wrapper">{!! $checkupms->render() !!}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection