@extends('layouts.main')

@section('content')

    <h1 class="h2">Activity</h1>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <h5 class="card-header"> {{ Html::link(route('checkups.show',$checkupd->check_up_m_id), 'Back') }} | ตั้งแผนวันเข้าดำเนินการ</h5>
                <div class="card-body">
                    <form method="POST" action="{{ route('checkups.setplanaction',$checkupd->id) }}">
                        @csrf
                        {{ Form::hidden('status','Created') }}
                        <div class="row">
                            <div class="col-6">
                                <label for="plan_date" class="form-label">Plan Date</label>
                                {{ Form::date('plan_date',null,['class'=>'form-control','placeholder'=>'ใส่ชื่อ','required'=>true]) }}
                            </div>                        
                            <div class="col-12">
                                {{ Form::submit('ตั้งแผน',['class'=>'btn btn-success']) }}
                            </div>
                        </div>
                    </form>
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>รอบการตรวจเช็ค / ปี</th>
                                    <td>{{ $checkupd->checkupm->name }} / {{ $checkupd->checkupm->year }}</td>
                               
                                    <th>สถานะการตรวจสอบ</th>
                                    <td>{{ $checkupd->status }}</td>
                                </tr>
                                <tr>
                                    <th>LACO CODE</th>
                                    <td>{{ $checkupd->asset->laco_code }}</td>
                               
                                    <th>Asset No.</th>
                                    <td>{{ $checkupd->asset->asst_no }}</td>
                                </tr>
                                <tr>
                                    <th>Computer Name</th>
                                    <td>{{ $checkupd->asset->com_name }}</td>
                                
                                    <th>Owner</th>
                                    <td>{{ $checkupd->asset->user_name }}</td>
                                </tr>
                                <tr>
                                    <th>Dep</th>
                                    <td>{{ $checkupd->asset->Dep }}</td>
                               
                                    <th>Service Tag</th>
                                    <td>{{ $checkupd->asset->service_tag }}</td>
                                </tr>
                                <tr>
                                    <th>LOB DESC</th>
                                    <td>{{ $checkupd->asset->LOB }} / {{ $checkupd->asset->DESC }}</td>
                                
                                    <th>ASSET SHIP DATE</th>
                                    <td>{{ $checkupd->asset->asset_ship_date }}</td>
                                </tr>
                                <tr>
                                    <th>CONTRACT END DATE</th>
                                    <td>{{ $checkupd->asset->contract_end_date }}</td>
                                
                                    <th>ปีเริ่มประกัน</th>
                                    <td>{{ $checkupd->asset->start_year }}</td>
                                </tr>
                                <tr>
                                    <th>อายุเครื่อง</th>
                                    <td>{{ date('Y') - $checkupd->asset->start_year }}</td>
                                
                                    <th>สถานะ</th>
                                    <td>{{ $checkupd->asset->com_status }}</td>
                                </tr>
                                <tr>
                                    <th>MSP</th>
                                    <td>{{ $checkupd->asset->use_msp }}</td>
                                
                                    <th>MSP Install DATE</th>
                                    <td>{{ $checkupd->asset->msp_install_date }}</td>
                                </tr>
                                <tr>
                                    <th>MAC Address</th>
                                    <td>{{ $checkupd->asset->mac_addr }}</td>
                                
                                    <th>OS</th>
                                    <td>{{ $checkupd->asset->win_ver }} / {{ $checkupd->asset->win_key }}</td>
                                </tr>
                                
                                <tr>
                                    <th>MS Office</th>
                                    <td>{{ $checkupd->asset->office_ver }}</td>
                                
                                    <th>Location</th>
                                    <td>{{ $checkupd->asset->location }}</td>
                                </tr>
                                <tr>
                                    <th>Note</th>
                                    <td>{{ $checkupd->asset->note }}</td>
                                
                                    <th>Status</th>
                                    <td>{{ $checkupd->asset->status }}</td>
                                </tr>
                            </tbody>
                        </table>
                     </div>
                    
                </div>
            </div>
        </div>
    </div>
@endsection
