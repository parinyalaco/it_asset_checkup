<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AssetsController;
use App\Http\Controllers\CheckUpsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('assets', AssetsController::class);

Route::get('/checkups', [CheckUpsController::class, 'index'])->name('checkups.index');
Route::get('/checkups/createplan', [CheckUpsController::class, 'createplan'])->name('checkups.createplan');
Route::post('/checkups/createplanAction', [CheckUpsController::class, 'createplanAction'])->name('checkups.createplanaction');
Route::get('/checkups/show/{id}', [CheckUpsController::class, 'show'])->name('checkups.show');
Route::get('/checkups/setplan/{id}', [CheckUpsController::class, 'setplan'])->name('checkups.setplan');
Route::post('/checkups/setplanAction/{id}', [CheckUpsController::class, 'setplanAction'])->name('checkups.setplanaction');
Route::get('/checkups/doit/{id}', [CheckUpsController::class, 'doit'])->name('checkups.doit');
Route::post('/checkups/doitAction/{id}', [CheckUpsController::class, 'doitAction'])->name('checkups.doitaction');

