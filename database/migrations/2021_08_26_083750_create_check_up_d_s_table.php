<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCheckUpDSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('check_up_d_s', function (Blueprint $table) {
            $table->id();
            $table->integer('check_up_m_id');
            $table->integer('asset_id');
            $table->date('plan_date')->nullable();
            $table->date('act_date')->nullable();
            $table->string('display',150)->nullable();
            $table->string('body', 150)->nullable();
            $table->string('case', 150)->nullable();
            $table->string('fan', 150)->nullable();
            $table->string('usb', 150)->nullable();
            $table->string('vga', 150)->nullable();
            $table->string('hdmi', 150)->nullable();
            $table->string('usbc', 150)->nullable();
            $table->string('cardreader', 150)->nullable();
            $table->string('keyboard', 150)->nullable();
            $table->string('mouse', 150)->nullable();
            $table->string('wifi', 150)->nullable();
            $table->string('lan', 150)->nullable();
            $table->string('internal', 150)->nullable();
            $table->string('hardware_other', 150)->nullable();
            $table->string('os', 150)->nullable();
            $table->string('msp', 150)->nullable();
            $table->string('software_note', 150)->nullable();
            $table->string('pic1')->nullable();
            $table->string('pic1_path')->nullable();
            $table->string('pic2')->nullable();
            $table->string('pic2_path')->nullable();
            $table->string('pic3')->nullable();
            $table->string('pic3_path')->nullable();
            $table->string('pic4')->nullable();
            $table->string('pic4_path')->nullable();
            $table->string('pic5')->nullable();
            $table->string('pic5_path')->nullable();
            $table->string('pic6')->nullable();
            $table->string('pic6_path')->nullable();
            $table->string('summary_note', 150)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('check_up_d_s');
    }
}
