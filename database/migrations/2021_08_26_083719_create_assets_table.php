<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->id();
            $table->string('laco_code', 30)->nullable();
            $table->string('asst_no', 30)->nullable();
            $table->string('com_name', 50);
            $table->string('user_name', 50)->nullable();
            $table->string('Dep', 30)->nullable();
            $table->string('service_tag', 50)->nullable();
            $table->string('LOB', 50)->nullable();
            $table->string('DESC', 100)->nullable();
            $table->date('asset_ship_date')->nullable();
            $table->date('contract_end_date')->nullable();
            $table->integer('start_year')->nullable();
            $table->integer('age')->nullable();
            $table->string('com_status', 30)->nullable();
            $table->string('use_msp', 10)->nullable();
            $table->date('msp_install_date')->nullable();
            $table->string('mac_addr',100)->nullable();
            $table->string('win_ver',50)->nullable();
            $table->string('win_key',100)->nullable();
            $table->string('office_ver',50)->nullable();
            $table->string('location',100)->nullable();
            $table->text('note')->nullable();
            $table->string('status',30)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets');
    }
}
