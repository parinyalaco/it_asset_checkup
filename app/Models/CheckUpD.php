<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CheckUpD extends Model
{
    use HasFactory;
    protected $fillable = [
        'check_up_m_id', 'asset_id', 'plan_date',
        'act_date',
        'display',
        'body',
        'case',
        'fan',
        'usb',
        'vga',
        'hdmi',
        'usbc',
        'cardreader',
        'keyboard',
        'mouse',
        'wifi',
        'lan',
        'internal',
        'hardware_other',
        'os',
        'msp',
        'software_note',
        'pic1',
        'pic1_path',
        'pic2',
        'pic2_path',
        'pic3',
        'pic3_path',
        'pic4',
        'pic4_path',
        'pic5',
        'pic5_path',
        'pic6',
        'pic6_path',
        'summary_note',
        'status'
    ];

    public function checkupm()
    {
        return $this->hasOne('App\Models\CheckUpM', 'id', 'check_up_m_id');
    }

    public function asset()
    {
        return $this->hasOne('App\Models\Asset', 'id',  'asset_id');
    }
}
