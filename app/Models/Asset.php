<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    use HasFactory;

    protected $fillable = ['laco_code', 'asst_no', 'com_name', 'user_name', 'Dep', 'service_tag','LOB','DESC','asset_ship_date',
    'contract_end_date', 'start_year', 'age', 'com_status',	'use_msp',	'msp_install_date', 'mac_addr', 'win_ver',	
    'win_key',	'office_ver',	'location',	'note', 'status'];
}
