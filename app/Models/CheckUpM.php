<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CheckUpM extends Model
{
    use HasFactory;
    protected $fillable = ['name','year','desc','status'];

    public function checkupds()
    {
        return $this->hasMany('App\Models\CheckUpD', 'check_up_m_id');
    }
}
