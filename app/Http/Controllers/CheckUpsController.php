<?php

namespace App\Http\Controllers;

use App\Models\Asset;
use App\Models\CheckUpM;
use App\Models\CheckUpD;
use Illuminate\Http\Request;

class CheckUpsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perPage = 25;
        $checkupms = CheckUpM::latest()->paginate($perPage);

        return view('checkups.index', compact('checkupms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createplan()
    {
        return view('checkups.createplan');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createplanAction(Request $request)
    {
        $requestData = $request->all();

        $checkupm = CheckUpM::create($requestData);

        //clone all asset for plan
        $assets = Asset::where('status','Active')->get();
        foreach ($assets as $assetObj) {
            $tmpCheckUpD = array();
            $tmpCheckUpD['check_up_m_id'] = $checkupm->id;
            $tmpCheckUpD['asset_id'] = $assetObj->id;
            $tmpCheckUpD['status'] = 'Pending';
            CheckUpD::create($tmpCheckUpD);    
        }

        return redirect('checkups')->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $checkupm = CheckUpM::findOrFail($id);

        $status = "";
        if(!empty($request->get('status'))){
            $status = $request->get('status');
        }
        $keyword = $request->get('search');
        $perPage = 25;

        $checkupdObj = new CheckUpD();

        if(!empty($keyword)){
            $assetseach = Asset::orWhere('com_name','like','%'. $keyword.'%')
                ->orWhere('Dep', 'like', '%' . $keyword . '%')
                ->orWhere('service_tag', 'like', '%' . $keyword . '%')
                ->pluck('id','id');
            if(!empty($status)){
                $checkupds = $checkupdObj
                    ->where('status', $status)
                ->where('check_up_m_id', $id)
                ->whereIn('asset_id', $assetseach)
                ->paginate($perPage);
            }else{
                $checkupds = $checkupdObj->where('check_up_m_id', $id)->whereIn('asset_id', $assetseach)->paginate($perPage);
            }
            
        }else{
            if (!empty($status)) {
                $checkupds = $checkupdObj->where('check_up_m_id', $id)->where('status', $status)->latest()->paginate($perPage);
            }else{
                $checkupds = $checkupdObj->where('check_up_m_id', $id)->latest()->paginate($perPage);
            }
        }        

        return view('checkups.show', compact('checkupm', 'checkupds'));
    }

    public function setplan($id){
        $checkupd = CheckUpD::findOrFail($id);
        return view('checkups.setplan', compact('checkupd'));
    }

    public function setplanAction(Request $request, $id)
    {
        $requestData = $request->all();

        $checkupd = CheckUpD::findOrFail($id);
        $checkupd->plan_date = $requestData['plan_date'];
        $checkupd->status = 'Planned';
        $checkupd->update();

        return redirect(route('checkups.show', $checkupd->check_up_m_id))->with('flash_message', ' added!');
    }

    public function doit($id)
    {
        $checkupd = CheckUpD::findOrFail($id);
        return view('checkups.doit', compact('checkupd'));
    }

    public function doitAction(Request $request, $id){
        $requestData = $request->all();

        $checkupd = CheckUpD::findOrFail($id);
        
        for ($i=1; $i <= 5; $i++) { 
            $chekname = 'pic'.$i.'f';    
            if ($request->hasFile($chekname)) {
                $image = $request->file($chekname);
                $name = md5($image->getClientOriginalName() . time()) . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('images/computers/' . $checkupd->check_up_m_id.'/'.$id);
                $image->move($destinationPath, $name);

                $requestData['pic'.$i] = $name;
                $requestData['pic' . $i . '_path'] = 'images/computers/' . $checkupd->check_up_m_id . '/' . $id  . "/" . $name;
            }
        }

        $requestData['status'] = 'Done';

        $checkupd->update($requestData);

        return redirect(route('checkups.show', $checkupd->check_up_m_id))->with('flash_message', ' added!');
    }

}
